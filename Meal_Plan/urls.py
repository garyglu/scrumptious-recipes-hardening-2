from django.urls import path


from Meal_Plan.views import (
    MealPlanCreateView,
    MealPlanDeleteView,
    MealPlanUpdateView,
    MealPlanDetailView,
    MealPlanListView,
)

urlpatterns = [
    path("MealPlan/", MealPlanListView.as_view(), name="MealPlan_list"),
    path("<int:pk>/", MealPlanDetailView.as_view(), name="MealPlan_detail"),
    path(
        "<int:pk>/delete/",
        MealPlanDeleteView.as_view(), name="MealPlan_delete"),

    path(
        "MealPlan/create/",
        MealPlanCreateView.as_view(), name="MealPlan_create"),
    path("<int:pk>/edit/", MealPlanUpdateView.as_view(), name="MealPlan_edit"),

]
