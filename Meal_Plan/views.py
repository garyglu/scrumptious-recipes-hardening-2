from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.shortcuts import redirect
from django.contrib.auth.mixins import LoginRequiredMixin


from Meal_Plan.models import MealPlan


class MealPlanListView(ListView):
    model = MealPlan
    template_name = "Meal_Plan/list.html"
    paginate_by = 2

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanDetailView(DetailView):
    model = MealPlan
    template_name = "Meal_Plan/detail.html"

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanCreateView(LoginRequiredMixin, CreateView):
    model = MealPlan
    template_name = "Meal_Plan/new.html"
    fields = ["name", "date", "owner", "recipes"]
    # success_url = reverse_lazy("MealPlan_list")

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("MealPlan_list")


class MealPlanUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlan
    template_name = "MealPlan/edit.html"
    fields = ["name", "date", "owner", "recipes"]
    success_url = reverse_lazy("MealPlan_list")

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)

    def get_success_url(self) -> str:
        return reverse_lazy("MealPlan_detail", args=[self.object.id])


class MealPlanDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlan
    template_name = "MealPlan/delete.html"
    success_url = reverse_lazy("Meal_Plan_list")

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)
